#!/usr/bin/env python
# coding=utf-8
from configparser import ConfigParser # canvi de nom de classe a Python3
from app import app

class readConfig(object):

    def __init__(self):
        self.config = ConfigParser()
        self.config.read('web.conf')

        self.cfg={}
        self.cfg['name'] = self.config.get('BRANDING', 'NAME')
        self.cfg['log_level'] = self.config.get('LOG', 'LEVEL')
        self.cfg['log_file'] = self.config.get('LOG', 'FILE')
        self.cfg['admin_users'] = self.config.get('AUTH', 'ADMIN_USERS').split(',')
        self.cfg['admin_groups'] = self.config.get('AUTH', 'ADMIN_GROUPS').split(',')
        self.cfg['dbhost'] = self.config.get('DATABASE', 'HOST')
        self.cfg['dbname'] = self.config.get('DATABASE', 'DBNAME')
        self.cfg['dbuser'] = self.config.get('DATABASE', 'USER')
        self.cfg['dbpass'] = self.config.get('DATABASE', 'PASS')
        try:
            self.cfg['devel_user']={'id':self.config.get('AUTH', 'DEVEL_USER'),
                                    'name':self.config.get('AUTH', 'DEVEL_USER'),
                                    'surname1':self.config.get('AUTH', 'DEVEL_USER'),
                                    'surname2':self.config.get('AUTH', 'DEVEL_USER'),
                                    'full_name':self.config.get('AUTH', 'DEVEL_USER'),
                                    'role': self.config.get('AUTH', 'DEVEL_ROLE'),
                                    'nif': '00000000A',
                                    'id_especialitat': self.config.get('AUTH', 'DEVEL_ID_ESPECIALITAT'),
                                    'especialitat':'Descr especialitat',
                                    'mail': 'noreplay@fake.com',
                                    'sesman_cookie': self.config.get('AUTH', 'DEVEL_USER')
                                }
        except:
            self.cfg['devel_user']=False
        print(self.cfg['admin_users'])
        print(self.cfg['admin_groups'])

    def getConfig(self):
        return self.cfg
