#!/usr/bin/env python
# coding=utf-8
import psycopg2
from app import app
from flask import g
from ..config.log import log

ram_users={}

class auth(object):
    def __init__(self):
        None

    def addDevelUser(self):
        if app.cfg['devel_user']:
            ram_users[app.cfg['devel_user']['id']]=app.cfg['devel_user']
            return app.cfg['devel_user']
        else:
            return False

    def existsUser(self,username):
        if username not in ram_users.keys():
            return False
        return True

    def getUser(self,username):
        if self.existsUser(username):
            return ram_users[username]
        return False

    def getUsernameFromCookie(self,cookie):
        username=[u['id'] for k,u in ram_users.items() if u['sesman_cookie']==cookie]
        return username[0] if len(username) else None

    def getSesmanCookie(self,username):
        if self.existsUser(username):
            return ram_users[username]['sesman_cookie']
        return False

    def addUser(self,username,userdata,sesman_cookie):
        ram_users[username]=self.createUser(username,userdata,sesman_cookie)
        return ram_users[username]

    def createUser(self,username,session_data,sesman_cookie):
        return {'id':username,
                'name':session_data['user_profile']['name'],
                'surname1':session_data['user_profile']['surname1'],
                'surname2':session_data['user_profile']['surname2'],
                'full_name':session_data['user_profile']['surname1']+' '+session_data['user_profile']['surname2']+', '+session_data['user_profile']['name'],
                'role':'admin' if username in app.cfg['admin_users'] else 'profe' if 'profes' in session_data['user_groups'] else 'user',
                'nif':session_data['user_profile']['nif'],
                'id_especialitat':session_data['user_profile']['id_especialitat'],
                'especialitat':session_data['user_profile']['especialitat'],
                'mail':session_data['user_profile']['email'],
                'sesman_cookie': sesman_cookie
            }

    def unregisteredUser(self):
        return {'id':'desconegut',
                'name':'No validat',
                'surname1':'',
                'surname2':'',
                'full_name':'',
                'role':'user',
                'nif':'',
                'id_especialitat':'',
                'especialitat':'',
                'mail':'',
                'sesman_cookie': ''
            }

    def logoutUser(self,username):
        ram_users.pop(username, None)
        g.user = None
        return True
