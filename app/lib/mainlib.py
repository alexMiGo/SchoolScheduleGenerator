#!/usr/bin/env python
# coding=utf-8
import time, json, requests
from app import app
import psycopg2

from flask import current_app
import re

from collections import OrderedDict

from pprint import pprint
from .database import *
from .hores import *

class horaris(object):
    def __init__(self):
        None

    def aules(self):
        return get_aules()

    def aules_dropdown(self):
        return get_aules_dropdown()

    def aules_profe_dropdown(self,codi_horaris):
        return get_aules_dropdown_profe(codi_horaris)

    def profe_info(self,profe):
        return get_profe_info(profe)

    def profes_dropdown(self):
        return get_profes_dropdown()

    def profes_dep_dropdown(self,departament):
        return get_profes_dropdown_departament(departament)

    def grups_dropdown(self):
        return get_grups_dropdown()

    def grups_dep_dropdown(self,departament):
        return get_grups_dropdown_departament(departament)

    def horari_simple(self,profe):
        horariProfe = get_horari_profe(profe)
        horari = []
        for horaridb in horariProfe:
            horari.append(horaridb)
        return horari

    '''
    For all methods below.
    Depending on the turn of a teacher, group or classroom we get morning, afternoon or all hours.
    If the query doesn't return any turn, we return an empty array (horari).
    Otherwise we return a dictionari with the following structure.
    [
        {"08:00 - 8:55":[
                            [some info, some other info],
                            [can be empty],
                            [some info],
                            [some info],
                            [some info]
                        ]
        },
        ...
    ]
    '''
    def horari_profe(self,profesor):
        horariProfe = get_horari_profe(profesor)
        horari = []
        hores = OrderedDict(sorted(get_hores().items()))
        for key,hora in hores.items():
            dictHores = {hora:[]}
            for dia in range(1,6):
                info = []
                for horaridb in horariProfe:
                    if dia == horaridb['dia'] and key == horaridb['hora']:
                        info.append(horaridb)
                dictHores[hora].append(info)
            horari.append(dictHores)
        return horari

    def horari_aula(self,aula):
        horariAula = get_horari_aula(aula)
        horari = []
        hores = OrderedDict(sorted(get_hores().items()))
        for key,hora in hores.items():
            dictHores = {hora:[]}
            for dia in range(1,6):
                info = []
                for horaridb in horariAula:
                    if dia == horaridb['dia'] and key == horaridb['hora']:
                        info.append(horaridb)
                dictHores[hora].append(info)
            horari.append(dictHores)
        return horari

    def horari_grup(self,grup):
        horariGrup = get_horari_grup(grup)
        horari = []
        hores = OrderedDict(sorted(get_hores().items()))
        for key,hora in hores.items():
            dictHores = {hora:[]}
            for dia in range(1,6):
                info = []
                for horaridb in horariGrup:
                    if dia == horaridb['dia'] and key == horaridb['hora']:
                        info.append(horaridb)
                dictHores[hora].append(info)
            horari.append(dictHores)
        return horari
