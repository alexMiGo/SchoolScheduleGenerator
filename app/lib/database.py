import psycopg2
import psycopg2.extras
import datetime
from app import app

def get_aules_dropdown():
    conn_string = "host='"+app.cfg['dbhost']+"' dbname='"+app.cfg['dbname']+"' user='"+app.cfg['dbuser']+"' password='"+app.cfg['dbpass']+"'"
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cursor.execute("SELECT DISTINCT aula FROM horaris WHERE aula IS NOT NULL ORDER BY aula;")
    return [[i['aula'],i['aula']] for i in cursor.fetchall()]

def get_aules_dropdown_profe(codi_horaris):
    conn_string = "host='"+app.cfg['dbhost']+"' dbname='"+app.cfg['dbname']+"' user='"+app.cfg['dbuser']+"' password='"+app.cfg['dbpass']+"'"
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cursor.execute("SELECT DISTINCT aula FROM horaris WHERE aula IS NOT NULL AND profe LIKE '" + codi_horaris + "' ORDER BY aula;")
    return [[i['aula'],i['aula']] for i in cursor.fetchall()]

def get_profe_info(profe):
    conn_string = "host='"+app.cfg['dbhost']+"' dbname='"+app.cfg['dbname']+"' user='"+app.cfg['dbuser']+"' password='"+app.cfg['dbpass']+"'"
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cursor.execute("SELECT codigo_horarios, CONCAT(apellido1,' ',apellido2,', ',nombre) AS nombre FROM profes WHERE codigo_profe LIKE '" + profe + "';")
    return cursor.fetchall()

def get_profes_dropdown():
    conn_string = "host='"+app.cfg['dbhost']+"' dbname='"+app.cfg['dbname']+"' user='"+app.cfg['dbuser']+"' password='"+app.cfg['dbpass']+"'"
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cursor.execute("SELECT codigo_profe, CONCAT(apellido1,' ',apellido2,', ',nombre) AS nombre FROM profes ORDER BY apellido1;")
    return [[i['codigo_profe'],i['nombre']] for i in cursor.fetchall()]

def get_profes_dropdown_departament(departament):
    conn_string = "host='"+app.cfg['dbhost']+"' dbname='"+app.cfg['dbname']+"' user='"+app.cfg['dbuser']+"' password='"+app.cfg['dbpass']+"'"
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cursor.execute("SELECT codigo_horarios, CONCAT(apellido1,' ',apellido2,', ',nombre) AS nombre\
                    FROM profes WHERE departamento LIKE '" + departament + "'\
                    ORDER BY apellido1;")
    return [[i['codigo_horarios'],i['nombre']] for i in cursor.fetchall()]

def get_grups_dropdown():
    conn_string = "host='"+app.cfg['dbhost']+"' dbname='"+app.cfg['dbname']+"' user='"+app.cfg['dbuser']+"' password='"+app.cfg['dbpass']+"'"
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cursor.execute("SELECT DISTINCT grupo FROM horaris WHERE grupo IS NOT NULL ORDER BY grupo;")
    return [[i['grupo'],i['grupo']] for i in cursor.fetchall()]

def get_grups_dropdown_departament(departament):
    conn_string = "host='"+app.cfg['dbhost']+"' dbname='"+app.cfg['dbname']+"' user='"+app.cfg['dbuser']+"' password='"+app.cfg['dbpass']+"'"
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cursor.execute("SELECT DISTINCT grupo FROM horaris WHERE grupo IS NOT NULL AND departamento LIKE '" + departament + "' ORDER BY aula;")
    return [[i['grup'],i['descripcio']] for i in cursor.fetchall()]

def get_horari_profe(profesor):
    conn_string = "host='"+app.cfg['dbhost']+"' dbname='"+app.cfg['dbname']+"' user='"+app.cfg['dbuser']+"' password='"+app.cfg['dbpass']+"'"
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cursor.execute("SELECT grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora FROM horaris\
                    WHERE profe LIKE '"+ profesor +"' ORDER BY hora,dia,materia;")
    return cursor.fetchall()

def get_horari_aula(aula):
    conn_string = "host='"+app.cfg['dbhost']+"' dbname='"+app.cfg['dbname']+"' user='"+app.cfg['dbuser']+"' password='"+app.cfg['dbpass']+"'"
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cursor.execute("SELECT grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora FROM horaris\
                    WHERE aula LIKE '"+ aula +"' ORDER BY hora,dia,profe,materia;")
    return cursor.fetchall()

def get_horari_grup(grupo):
    conn_string = "host='"+app.cfg['dbhost']+"' dbname='"+app.cfg['dbname']+"' user='"+app.cfg['dbuser']+"' password='"+app.cfg['dbpass']+"'"
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    cursor.execute("SELECT grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora FROM horaris\
                    WHERE grupo LIKE '"+ grupo +"' ORDER BY hora,dia,profe,materia;")
    return cursor.fetchall()
