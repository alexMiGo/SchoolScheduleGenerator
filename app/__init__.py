#!/usr/bin/env python
# coding=utf-8
from flask import Flask, send_from_directory, Blueprint, render_template, redirect, request, flash, url_for
import os

app = Flask(__name__,static_url_path='')

'''
App secret key for encrypting cookies
'''
app.secret_key = "\xe0\x98\xea5\xfa\x89\x93\xf4\xcf\xaf\xe2'\x1f`\x85#(\x03)\xb1\x05P\xaf\xd8"

'''
App config
'''
from .config.config import readConfig
c=readConfig()
app.cfg=c.getConfig()

'''
App logs
'''
from .config.log import log

'''
Debug should be removed on production!
'''
app.debug = True
if app.debug:
    log.warning('Debug mode: {}'.format(app.debug))
else:
    log.info('Debug mode: {}'.format(app.debug))

'''
Serve static files
'''
@app.route('/horaris/bower_components/<path:path>')
def send_bower(path):
    return send_from_directory(os.path.join(app.root_path, 'bower_components'), path)

@app.route('/horaris/build/<path:path>')
def send_build(path):
    return send_from_directory(os.path.join(app.root_path, 'bower_components/gentelella/build'), path)

@app.route('/horaris/vendors/<path:path>')
def send_vendors(path):
    return send_from_directory(os.path.join(app.root_path, 'bower_components/gentelella/vendors'), path)

@app.route('/horaris/static/<path:path>')
def send_static_js(path):
    return send_from_directory(os.path.join(app.root_path, 'static'), path)

@app.route('/horaris/dist/<path:path>')
def send_dist(path):
    return send_from_directory(os.path.join(app.root_path, 'dist'), path)

@app.route('/horaris/js/<path:path>')
def send_js(path):
    return send_from_directory(os.path.join(app.root_path, 'js'), path)

@app.route('/horaris/css/<path:path>')
def send_css(path):
    return send_from_directory(os.path.join(app.root_path, 'css'), path)

'''
Import all views
'''
from .views import LoginViews
from .views import ProfeViews
from .views import Errors
