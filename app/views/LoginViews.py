#!/usr/bin/env python
# coding=utf-8
from flask import render_template, Response, request, redirect, url_for, g, Blueprint
from app import app
import json
import requests
from ..config.log import log
from ..auth.authentication import *
au=auth()

appuser = Blueprint('appuser', __name__, url_prefix='/horaris')
@appuser.before_request
def before_request():
    devel_user=au.addDevelUser()
    g.user=devel_user
    return

@appuser.route('/', methods=['GET'])
def horari_actual():
    if g.user['role'] == "user":
        accion='Grups'
        horari = app.h.horari_grup(g.user['id_especialitat'])
    else:
        accion='Profes'
        try:
            profeInfo = app.h.profe_info(g.user['id'])[0]
            horari=app.h.horari_profe(profeInfo['codigo_horarios'])
        except IndexError:
            horari=[]
    return render_template('pages/horariPersonal.html', g=g, action=accion, header="El teu horari", horari=horari)

@appuser.route('/about', methods=['GET'])
def about():
	return render_template('pages/about.html', g=g, title="About")

@appuser.route('/logout')
def logout():
    #TODO
    return

app.register_blueprint(appuser)
