#!/usr/bin/env python
# coding=utf-8
from flask import render_template, Response, request, redirect, url_for, Blueprint, redirect, g, request
from app import app
import requests
import time
import json

from ..lib.hores import *
from ..auth.authentication import *
from collections import OrderedDict

'''BLUEPRINT FOR PROFES'''
au=auth()

appprofes = Blueprint('appprofes', __name__, url_prefix='/horaris/profes')
@appprofes.before_request
def before_request():
    devel_user=au.addDevelUser()
    g.user=devel_user
    return

'''FUNCTIONS'''

# Search for a teacher, group or classroom in the array dropdown
def is_present(dropdown, name):
    for element in dropdown:
        if element[0] == name:
            return True
    return False

'''VIEWS'''
@appprofes.route('/aules', methods=['GET'])
@appprofes.route('/aules/<aula>', methods=['GET'])
def profe_aules_general(aula=False):
    accion = 'Aules'
    if g.user['role'] == 'admin' or g.user['id_especialitat'] in app.cfg['admin_groups']:
        dropdown = app.h.aules_dropdown()
    else:
        try:
            profeInfo = app.h.profe_info(g.user['id'])[0]
            dropdown = app.h.aules_profe_dropdown(profeInfo['codigo_horarios'])
        except IndexError:
            dropdown = []
    if not aula:
        aula='Totes'
        horari=[]
    else:
        if is_present(dropdown,aula):
            horari=app.h.horari_aula(aula)
        else:
            return redirect('horaris/profes/aules')
    return render_template('pages/horaris.html', g=g, action=accion, title=aula, title2=aula, header="Horaris aules", dropdown=dropdown, horari=horari)

@appprofes.route('/professors', methods=['GET'])
@appprofes.route('/professors/<profe>', methods=['GET'])
def profe_profe_general(profe=False):
    accion = 'Profes'
    if g.user['role'] == 'admin' or g.user['id_especialitat'] in app.cfg['admin_groups']:
        dropdown = app.h.profes_dropdown()
    else:
        dropdown = app.h.profes_dep_dropdown(g.user['id_especialitat'])
    if not profe:
        profeNom='Totes'
        profeDetail = ''
        horari=[]
    else:
        if is_present(dropdown,profe):
            try:
                profeInfo = app.h.profe_info(profe)[0]
                horari=app.h.horari_profe(profeInfo['codigo_horarios'])
                profeNom = profeInfo['nombre']
                profeDetail = profeNom + ' (' + profeInfo['codigo_horarios'] + ')'
            except IndexError:
                profeNom = profe
                profeDetail = profe
        else:
            return redirect('horaris/profes/professors')
    return render_template('pages/horaris.html', g=g, action=accion, codi_xarxa=profe, title=profeNom, title2=profeDetail, header="Horaris professor", dropdown=dropdown, horari=horari)

@appprofes.route('/grups', methods=['GET'])
@appprofes.route('/grups/<grup>', methods=['GET'])
def profe_grup_general(grup=False):
    accion = 'Grups'
    if g.user['role'] == 'admin' or g.user['id_especialitat'] in app.cfg['admin_groups']:
        dropdown = app.h.grups_dropdown()
    else:
        dropdown = app.h.grups_dep_dropdown(g.user['id_especialitat'])
    if not grup:
        grupNom='Totes'
        grupDetail=''
        horari=[]
    else:
        if is_present(dropdown,grup):
            grupNom=grup
            horari=app.h.horari_grup(grup)
            grupDetail = grup
        else:
            return redirect('horaris/profes/grups')
    return render_template('pages/horaris.html', g=g, action=accion, title=grupNom, title2=grupDetail, header="Horaris grup", dropdown=dropdown, horari=horari)

@appprofes.route('/espais_lliures', methods=['GET'])
def combina_horaris():
    hores = OrderedDict(sorted(get_hores().items()))
    if g.user['role'] == 'admin' or g.user['id_especialitat'] in app.cfg['admin_groups']:
        dropdown = app.h.profes_dropdown()
    else:
        dropdown = app.h.profes_dep_dropdown(g.user['id_especialitat'])
    return render_template('pages/combinaHoraris.html', g=g, title="Combina horaris", title2="Horari Combinat", dropdown=dropdown, hores=hores)

@appprofes.route('/info/<profe>', methods=['POST'])
def informacio_profe(profe):
    profeInfo = app.h.profe_info(profe)[0]
    horari=app.h.horari_simple(profeInfo['codigo_horarios'])
    profeNom=profeInfo['nombre']
    return json.dumps({'codi_xarxa':profe,'nom':profeNom,'horari':horari}),200,{'Content-Type':'application/json'}

app.register_blueprint(appprofes)
