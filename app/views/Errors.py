#!/usr/bin/env python
# coding=utf-8
from flask import render_template, Response, request, redirect, url_for, g, Blueprint
from app import app
import json
import requests
from ..config.log import log
from ..auth.authentication import *
au=auth()

@app.before_request
def before_request():
    devel_user=au.addDevelUser()
    g.user=devel_user
    return

@app.errorhandler(404)
def error_404(e):
    return render_template('pages/error.html', g=g, title="Error 404", error_type="Error 404!", description="La pàgina solicitada no existeix."), 404

@app.errorhandler(500)
def error_500(e):
    return render_template('pages/error.html', g=g, title="Error 500", error_type="Error 500!", description="S'ha produït un error inesperat."), 500
