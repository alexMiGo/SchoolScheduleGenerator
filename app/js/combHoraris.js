var profes = [];

function isEmpty( el ){
    return !$.trim(el.html())
}

$('.btn-print').click(function() {
  var restorepage = $('body').html();
  var printcontent = $('.x_content').clone();
  $('body').empty().html(printcontent);
  window.print();
  $('body').html(restorepage);
});


/*
* This function triggers when we change the value on the select in template 'combinaHoraris.html'
* Ajax makes a petiton to the specified url and gets the data.
* Then if the teacher isn't in the profes array we add it.
* With the data of the teacher we can modify the html and insert the teacher to the pertinent td of a table.
*/
$('.selectpicker').change(function() {
  var value = $('.selectpicker').val();
  $.ajax({
    url: '/horaris/profes/info/' + value,
    type: 'POST',
    success: function(data) {
      if (profes.indexOf(data.codi_xarxa) == -1) {
        profes.push(data.codi_xarxa);
        $('#llistaProf').append("<a id='"+data.codi_xarxa+"' class='btn btn-default' onclick='remove_teacher(this.id)'>" + data.nom + "&nbsp;<span class='glyphicon glyphicon-remove'></span></a>");
        horari = data.horari;
        for (var i = 0; i < horari.length; i++) {
          $('td#'+horari[i].hora+horari[i].dia).append("<p class='"+data.codi_xarxa+"'><img src='/horaris/static/img/fakeuser.png' alt='...' class='img-circle' height='40px' width='40px'>&nbsp;" + data.nom + "</p>");
          $('td#'+horari[i].hora+horari[i].dia).removeClass("success").addClass("danger");
        }
      }
    },
    error: function warn() {
      alert("S'ha produït un error inesperat")
    }
  })
});

/*
* Deletes the 'a' tag with id 'codi_xarxa' and all 'p' tags with class 'codi_xarxa'.
* If the td it's empty we change the class.
*/
function remove_teacher(codi_xarxa) {
  var index = profes.indexOf(codi_xarxa);
  if (index != -1) {
    profes = profes.slice(0,index).concat(profes.slice(index + 1,profes.length));
    $("a#"+codi_xarxa).remove();
    $("p."+codi_xarxa).remove();
    for (var i = 1; i <= 12; i++) {
      for (var j = 1; j <= 5; j++) {
        if (isEmpty($('td#'+i+j))) {
            $('td#'+i+j).removeClass("danger").addClass("success");
        }
      }
    }
  }
}
