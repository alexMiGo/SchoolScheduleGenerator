// Generate a colour by a given string.
function stringToColour(str) {
  str = "010" + str + "010";
  var hash = 0;
  for (var i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  var colour = '#';
  for (var i = 0; i < 3; i++) {
    var value = (hash >> (i * 8)) & 0xFF;
    colour += ('00' + value.toString(16)).substr(-2);
  }
  colour = colour.replace(/^\s*#|\s*$/g, '');
  // convert 3 char codes --> 6, e.g. `E0F` --> `EE00FF`
  if(colour.length == 3){
      colour = colour.replace(/(.)/g, '$1$1');
  }

  var r = parseInt(colour.substr(0, 2), 16),
      g = parseInt(colour.substr(2, 2), 16),
      b = parseInt(colour.substr(4, 2), 16);
  // Modify the color to be more bright.
  var percentage = 50;
  return '#' +
     ((0|(1<<8) + r + (256 - r) * percentage / 100).toString(16)).substr(1) +
     ((0|(1<<8) + g + (256 - g) * percentage / 100).toString(16)).substr(1) +
     ((0|(1<<8) + b + (256 - b) * percentage / 100).toString(16)).substr(1);
}

function changeColor(materia) {
  var color = stringToColour(materia);
  $('.' + materia).css('background-color', color);
}

// Change the background color of the tables with same class subjects.
jQuery(document).ready(function() {
  var materias = $(".change_color").map(function() {
    return $( this ).text();
  }).get();
  for (var materia in materias) {
    if (materias[materia].includes('(')) {
      changeColor(materias[materia].split('(')[1].split(')')[0].trim());
    } else {
      changeColor(materias[materia].trim());
    }
  }
});
