$('.validate-action').on('click', function () {
    console.log($(this).closest("div").attr('data-nif'))
    var nif=$(this).closest("div").attr('data-nif')
    var pathArray = window.location.pathname.split( '/' )
    console.log($(nif))
    $.ajax({
      url: '/admin/validar/'+pathArray[2],
      type: 'POST',
      data: JSON.stringify({'nifs':[nif]}),
      dataType: "json",
      contentType: "application/json",
      accepts: "application/json",
      success: function(data){
          console.log(data)
          $('#img-'+nif).parent('div').parent('div').remove();
    },
    error: function(data){
      console.log('error!!!!')
    }
  })
});

$('.btn-validar-pagina').on('click', function () {
    var pathArray = window.location.pathname.split( '/' )
    var nifs=[]
    fotoArray = $('.col-md-55')
    $(fotoArray).each(function(index) {
      nifs.push($(this).attr('id'))
    })
    //~ console.log(nifs)
    $.ajax({
      url: '/admin/validar/'+pathArray[2],
      type: 'POST',
      data: JSON.stringify({'nifs':nifs}),
      dataType: "json",
      contentType: "application/json",
      accepts: "application/json",
      success: function(data){
          console.log(data.length)
          for (var i = 0; i < data.length; i++ ) {
            console.log('ELIMINADA: ' + '.' + data[i])
            $("#"+data[i]).remove()
          }
          console.log('out of for')
          console.log(data.length + '=!=' + fotoArray.length)
          if (data.length == fotoArray.length) {
            console.log("longituds iguals");
            new PNotify({
              title: 'Pàgina validada correctament',
              text: 'Clica per carregar més fotos',
              //opacity: 1,
              type: 'success',
              icon: 'glyphicon glyphicon-info-sign',
              hide: false,
              confirm: {
                confirm: true,
                buttons: [{
                  text: 'Ok',
                  addClass: 'btn-primary',
                  click: function(notice) {
                    //alert('Ok, cool.');
                    //notice.remove();
                    location.reload()
                  }
                },
                null]
              },
              buttons: {
                  closer: false,
                  sticker: false
              },
              history: {
                  history: false
              },
              addclass: 'stack-modal',
              stack: {'dir1': 'down', 'dir2': 'right', 'modal': true},
              before_open: function(PNotify) {
                PNotify.css({
                  "top":"100px",
                  "left": ($(window).width() / 2) - (pnotify.width() / 2)
                });
                //PNotify.get().css(get_center_pos(PNotify.get().width()));
              }
            });
          }
          else {
            console.log("longituds diferents")
            new PNotify( {
              title: 'Ups!',
              text: 'algunes fotografies no s\'han pogut validar',
              type: 'error'
            })
          }
      },
      error: function(data){
        console.log('error!!!!')
      }
    })
});

$('.rename-action').on('click', function () {
  var current_nif=$(this).closest("div").attr('data-nif')
  //~ var current_folder=$(this).parent().parent().parent().prev().attr('src').split('/')[2]
  var pathArray = window.location.pathname.split( '/' )
  var notice = new PNotify({
    title: 'Editar DNI',
    text: 'Introdueix el DNI correcte:',
    icon: 'glyphicon glyphicon-question-sign',
    hide: false,
    confirm: {
        prompt: true,
        prompt_default: $('<div/>').text(current_nif).html()
    },
    buttons: {
        closer: false,
        sticker: false
    },
    history: {
        history: false
    },
    addclass: 'stack-modal',
    stack: {'dir1': 'down', 'dir2': 'right', 'modal': false}
  });
  notice.get().on('pnotify.confirm', function(e, notice, new_nif) {
    //notice.remove()
    console.log(new_nif)
    notice.cancelRemove().update({
        title: 'Nou DNI',
        text: 'Has introduït el DNI ' + $('<div/>').text(new_nif).html() + '.',
        icon: true,
        type: 'info',
        hide: true,
        confirm: {
            prompt: false
        },
        buttons: {
            closer: true,
            sticker: true
        }
    });
    $.ajax({
      url: '/admin/rename/'+current_nif+'/'+new_nif,
      type: 'GET',
      data: new_nif,
      accepts: "application/json",
      success: function(data){
          console.log(data)
          $('#img-'+current_nif).parent('div').parent('div').remove();
        },
      error: function(data){
        console.log('error!!!!')
      }
    })
  });
});

$('.delete-action').on('click', function () {
    console.log($(this).closest("div").attr('data-nif'))
    var nif=$(this).closest("div").attr('data-nif')
    var pathArray = window.location.pathname.split( '/' )
    $.ajax({
      url: '/admin/remove/'+pathArray[4]+'/'+nif,
      type: 'GET',
      success: function(data){
          console.log('Removed:'+data)
          console.log(pathArray[3])
          if(pathArray[2]==='orla'){
              console.log(pathArray[3])
              $('#img-'+nif).attr('src','/static/img/user_missing.jpg');
          }else{
            $('#img-'+nif).parent('div').parent('div').remove();
          }
        },
        error: function(data){
          console.log('error!!!!')
        }
    })
});

$('#reset-action').on('click', function () {
    console.log('Database reset button')
    $(".fa-refresh").addClass('fa-spin')
    $.ajax({
      url: '/admin/reset',
      type: 'GET',
      success: function(data){
          console.log('Database reset')
            //~ new PNotify({
              //~ title: 'Database reset',
              //~ text: 'Successful',
              //~ //opacity: 1,
              //~ type: 'success',
              //~ icon: 'glyphicon glyphicon-info-sign',
              //~ hide: false,
              //~ addclass: 'stack-modal',
              //~ stack: {'dir1': 'down', 'dir2': 'right', 'modal': true},
                //~ //PNotify.get().css(get_center_pos(PNotify.get().width()));
              //~ });
              $(".fa-refresh").removeClass('fa-spin')
        },
        error: function(data){
          console.log('ERROR: Something went wrong while resetting database')
          $(".fa-refresh").removeClass('fa-spin')
        }
    })
});

$(".fa-image").on('click',function(){
  //~ var opts = {
       //~ title: "Over Here",
       //~ text: "Check me out. I'm in a different stack.",
       //~ addclass: "stack-topleft",
   //~ };
       //~ opts.title = "Good News Everyone";
       //~ opts.text = "I've invented a device that bites shiny metal asses.";
       //~ opts.type = "success";
   //~ new PNotify(opts);

});
