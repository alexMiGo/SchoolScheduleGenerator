#!flask/bin/python

import time
from app import app

from gevent.wsgi import WSGIServer
from gevent import monkey; monkey.patch_all(thread=False)
from app import app

from app.lib.mainlib import horaris
app.h=horaris()

if __name__ == "__main__":
	http_server = WSGIServer(('0.0.0.0', 9999), app)
	http_server.serve_forever()
