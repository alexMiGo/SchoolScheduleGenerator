FROM fedora:25
RUN dnf update -y
RUN dnf install python3-pip python3-devel.x86_64 python3.x86_64 -y
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
RUN mkdir /horaris
ADD requirements.pip3 /horaris/requirements.pip3
WORKDIR /horaris
RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.pip3
EXPOSE 9999
CMD ["python3","run.py"]
