CREATE TABLE profes
(
	codigo_profe character varying(10) UNIQUE,
	codigo_horarios character varying(5),
	apellido1 character varying(25),
	apellido2 character varying(25),
	nombre character varying(15),
	departamento char(3),
	PRIMARY KEY(codigo_profe)
);

INSERT INTO profes (codigo_profe,codigo_horarios,apellido1,apellido2,nombre,departamento) VALUES ('myuser','P001','Profe1','Profe1','Profe1','INF');
INSERT INTO profes (codigo_profe,codigo_horarios,apellido1,apellido2,nombre,departamento) VALUES ('myuser2','P002','Profe2','Profe2','Profe2','DIR');

CREATE TABLE horaris
(
  id SERIAL UNIQUE,
  grupo character varying(5),
  codigo_profe character varying(10) REFERENCES profes(codigo_profe),
  profe character varying(5),
  materia character varying(5),
  nombre_materia character varying(25),
  aula character varying(5),
  dia integer,
  hora integer,
  PRIMARY KEY (id)
);

INSERT INTO horaris (grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora) VALUES ('1A','myuser','P001','INF','Informatica 1','1AULA',1,1);
INSERT INTO horaris (grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora) VALUES ('1A','myuser','P001','INF','Informatica 1','1AULA',1,2);
INSERT INTO horaris (grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora) VALUES ('1A','myuser','P001','INF','Informatica 1','1AULA',2,4);
INSERT INTO horaris (grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora) VALUES ('1A','myuser','P001','INF','Informatica 1','1AULA',2,5);
INSERT INTO horaris (grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora) VALUES ('1A','myuser','P001','INF','Informatica 1','1AULA',3,6);
INSERT INTO horaris (grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora) VALUES ('1A','myuser','P001','INF','Informatica 1','1AULA',3,7);
INSERT INTO horaris (grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora) VALUES ('1A','myuser','P001','INF','Informatica 1','1AULA',4,9);
INSERT INTO horaris (grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora) VALUES ('1A','myuser','P001','INF','Informatica 1','1AULA',4,10);
INSERT INTO horaris (grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora) VALUES ('1A','myuser','P001','INF','Informatica 1','1AULA',5,11);
INSERT INTO horaris (grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora) VALUES ('1A','myuser','P001','INF','Informatica 1','1AULA',5,12);
INSERT INTO horaris (grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora) VALUES ('2A','myuser2','P002','MAT','Matematicas 2','2AULA',1,5);
INSERT INTO horaris (grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora) VALUES ('2A','myuser2','P002','MAT','Matematicas 2','2AULA',1,6);
INSERT INTO horaris (grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora) VALUES ('2A','myuser2','P002','MAT','Matematicas 2','2AULA',2,4);
INSERT INTO horaris (grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora) VALUES ('2A','myuser2','P002','MAT','Matematicas 2','2AULA',2,5);
INSERT INTO horaris (grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora) VALUES ('2A','myuser2','P002','MAT','Matematicas 2','2AULA',3,1);
INSERT INTO horaris (grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora) VALUES ('2A','myuser2','P002','MAT','Matematicas 2','2AULA',3,2);
INSERT INTO horaris (grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora) VALUES ('2A','myuser2','P002','MAT','Matematicas 2','2AULA',4,4);
INSERT INTO horaris (grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora) VALUES ('2A','myuser2','P002','MAT','Matematicas 2','2AULA',4,5);
INSERT INTO horaris (grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora) VALUES ('2A','myuser2','P002','MAT','Matematicas 2','2AULA',5,11);
INSERT INTO horaris (grupo,codigo_profe,profe,materia,nombre_materia,aula,dia,hora) VALUES ('2A','myuser2','P002','MAT','Matematicas 2','2AULA',5,12);
